export const state = () => ({
  tipoPlatoSeleccionado: 1,
  listaTipoPlatos: [
    {
      src: "carousel/ARROCES.png",
      tipo: "Arroces"
    },
    {
      src: "carousel/CEVICHES.png",
      tipo: "Ceviches"
    },
    {
      src: "carousel/CHICHARRONES.png",
      tipo: "Chicharrones"
    },
    {
      src: "carousel/CHILCANOS.png",
      tipo: "Chilcanos"
    },
    {
      src: "carousel/CHUPES.png",
      tipo: "Chupes"
    },
    {
      src: "carousel/SUDADOS.png",
      tipo: "Sudados"
    },
    {
      src: "carousel/FRITURAS.png",
      tipo: "Frituras"
    },
    {
      src: "carousel/FUENTES.png",
      tipo: "Fuentes"
    },
    {
      src: "carousel/LECHES.png",
      tipo: "Leches"
    },
    {
      src: "carousel/PLATOS-PARA-EVENTOS.png",
      tipo: "Para Eventos"
    },
    {
      src: "carousel/PARIHUELA.png",
      tipo: "Parihuelas"
    },
    {
      src: "carousel/PLATOS-DOBLES.png",
      tipo: "Platos Dobles"
    },
    {
      src: "carousel/PORCIONES.png",
      tipo: "Porciones"
    }
  ],
  listaPlatos: [
    [
      {
        nombre: "Arroz con Mariscos",
        precio: "15.00",
        src: "platos/ARROCES/ARROZ-CON-MARISCO.png"
      },
      {
        nombre: "Chaufa de Mariscos",
        precio: "16.00",
        src: "platos/ARROCES/CHAUFA-DE-MARISCOS.png"
      },
      {
        nombre: "Chaufa de Pescado",
        precio: "15.00",
        src: "platos/ARROCES/CHAUFA-DE-PESCADO.png"
      },
      {
        nombre: "Chaufa de Trucha",
        precio: "15.00",
        src: "platos/ARROCES/CHAUFA-DE-TRUCHA.png"
      }
    ],
    [
      {
        nombre: "Ceviche de Mariscos",
        precio: "18.00",
        src: "platos/CEVICHES/CEVICHE-DE-MARISCOS.png"
      },
      {
        nombre: "Ceviche de Pescado",
        precio: "15.00",
        src: "platos/CEVICHES/CEVICHE-SIMPLE.png"
      },
      {
        nombre: "Ceviche de Trucha",
        precio: "16.00",
        src: "platos/CEVICHES/CEVICHE-DE-TRUCHA.png"
      }
    ],
    [
      {
        nombre: "Chicharrón de Mariscos",
        precio: "18.00",
        src: "platos/CHICHARRONES/CHICHARRON-DE-MARISCOS.png"
      },
      {
        nombre: "Chicharrón de Pescado",
        precio: "15.00",
        src: "platos/CHICHARRONES/CHICHARRON-DE-PESCADO.png"
      },
      {
        nombre: "Chicharrón de Pota",
        precio: "10.00",
        src: "platos/CHICHARRONES/CHICHARRON-DE-POTA.png"
      },
      {
        nombre: "Chicharrón Mixto",
        precio: "15.00",
        src: "platos/CHICHARRONES/CHICHARRON-MIXTO.png"
      },
      {
        nombre: "Chicharrón de Trucha",
        precio: "18.00",
        src: "platos/CHICHARRONES/CHICHARRON-DE-TRUCHA.png"
      }
    ],
    [
      {
        nombre: "Chilcano de Cabrilla",
        precio: "18.00",
        src: "platos/CHILCANOS/CHILCANO-DE-CABRILLA.png"
      },
      {
        nombre: "Chilcano de Filete",
        precio: "15.00",
        src: "platos/CHILCANOS/CHILCANO-DE-FILETE.png"
      },
      {
        nombre: "Chilcano de Tramboyo",
        precio: "18.00",
        src: "platos/CHILCANOS/CHILCANO-DE-TRAMBOYO.png"
      },
      {
        nombre: "Chilcano Especial",
        precio: "10.00",
        src: "platos/CHILCANOS/CHILCANO-ESPECIAL.png"
      },
      {
        nombre: "Chilcano Simple",
        precio: "6.00",
        src: "platos/CHILCANOS/CHILCANO-SIMPLE.png"
      }
    ],
    [
      {
        nombre: "Chupe de Mariscos",
        precio: "18.00",
        src: "platos/CHUPES/CHUPE-DE-MARISCOS.png"
      },
      {
        nombre: "Chupe de Pescado",
        precio: "18.00",
        src: "platos/CHUPES/CHUPE-DE-PESCADO.png"
      }
    ],
    [
      {
        nombre: "Sudado de Cabrilla",
        precio: "18.00",
        src: "platos/SUDADOS/SUDADO-DE-CABRILLA.png"
      },
      {
        nombre: "Sudado de Filete",
        precio: "15.00",
        src: "platos/SUDADOS/SUDADO-DE-FILETE.png"
      },
      {
        nombre: "Sudado de Tramboyo",
        precio: "18.00",
        src: "platos/SUDADOS/SUDADO-DE-TRAMBOYO.png"
      },
      {
        nombre: "Sudado de Trucha",
        precio: "18.00",
        src: "platos/SUDADOS/SUDADO-DE-TRUCHA.png"
      }
    ],
    [
      {
        nombre: "Cabrilla Frita",
        precio: "18.00",
        src: "platos/FRITURAS/CABRILLA-FRITA.png"
      },
      {
        nombre: "Filete Frito",
        precio: "15.00",
        src: "platos/FRITURAS/FILETE-FRITO.png"
      },
      {
        nombre: "Tramboyo Frito",
        precio: "18.00",
        src: "platos/FRITURAS/TRAMBOYO-FRITO.png"
      },
      {
        nombre: "Trucha Frita",
        precio: "12.00",
        src: "platos/FRITURAS/TRUCHA-FRITA.png"
      }
    ],
    [
      {
        nombre: "Fuente de Ceviche Grande",
        precio: "70.00",
        src: "platos/FUENTES/FUENTE-DE-CEVICHE-GRANDE.png"
      },
      {
        nombre: "Fuente de Ceviche Mediana",
        precio: "50.00",
        src: "platos/FUENTES/FUENTE-DE-CEVICHE-MEDIANA.png"
      },
      {
        nombre: "Fuente de Chicharrón Grande",
        precio: "70.00",
        src: "platos/FUENTES/FUENTE-DE-CHICHARRON-GRANDE.png"
      },
      {
        nombre: "Fuente de Chicharrón Mediano",
        precio: "50.00",
        src: "platos/FUENTES/FUENTE-DE-CHICHARRON-MEDIANA.png"
      }
    ],
    [
      {
        nombre: "Leche con Chicharrón de Pota",
        precio: "10.00",
        src: "platos/LECHES/LECHE-CON-CHICHARRON-DE-POTA.png"
      },
      {
        nombre: "Leche con Mariscos",
        precio: "10.00",
        src: "platos/LECHES/LECHE-CON-MARISCOS.png"
      },
      {
        nombre: "Leche de Tigre",
        precio: "7.00",
        src: "platos/LECHES/LECHE-DE-TIGRE.png"
      }
    ],
    [
      {
        nombre: "Ají de Gallina",
        precio: "10.00",
        src: "platos/PLATOS_PARA_EVENTOS/AJI-DE-GALLINA.png"
      },
      {
        nombre: "Asado de Res con Puré",
        precio: "12.00",
        src: "platos/PLATOS_PARA_EVENTOS/ASADO-DE-RES-CON-PURE.png"
      },
      {
        nombre: "Chancho al Palo con Hierbas Aromáticas",
        precio: "20.00",
        src:
          "platos/PLATOS_PARA_EVENTOS/CHANCHO-AL-PALO-CON-HIERBAS-AROMATICAS.png"
      },
      {
        nombre: "Chuleta de Chancho c/ Pastel de Papa o Rocoto Relleno",
        precio: "20.00",
        src:
          "platos/PLATOS_PARA_EVENTOS/CHULETA-DE-CHANCHO-CON-PASTEL-DE-PAPA-O-ROCOTO-RELLENO.png"
      },
      {
        nombre: "Chuleta de Chancho",
        precio: "16.00",
        src: "platos/PLATOS_PARA_EVENTOS/CHULETA-DE-CHANCHO.png"
      },
      {
        nombre: "Cuy Chactado (1/2)",
        precio: "18.00",
        src: "platos/PLATOS_PARA_EVENTOS/CUY-CHACTADO---MEDIO.png"
      },
      {
        nombre: "Pachamanca a la Olla",
        precio: "18.00",
        src: "platos/PLATOS_PARA_EVENTOS/PACHAMANCA-A-LA-OLLA.png"
      },
      {
        nombre: "Parrilla de Carne",
        precio: "15.00",
        src: "platos/PLATOS_PARA_EVENTOS/PARRILLA-DE-CARNE.png"
      },
      {
        nombre: "Parrilla de Pechuga",
        precio: "14.00",
        src: "platos/PLATOS_PARA_EVENTOS/PARRILLA-DE-PECHUGA.png"
      },
      {
        nombre: "Parrilla Mixta (Carne, Pollo y Hot Dog)",
        precio: "25.00",
        src: "platos/PLATOS_PARA_EVENTOS/PARRILLA-MIXTA.png"
      },
      {
        nombre: "Pollo a la Plancha",
        precio: "14.00",
        src: "platos/PLATOS_PARA_EVENTOS/POLLO-A-LA-PLANCHA.png"
      },
      {
        nombre: "Trucha Frita con Puré",
        precio: "14.00",
        src: "platos/PLATOS_PARA_EVENTOS/TRUCHA-FRITA-CON-PURE.png"
      }
    ],
    [
      {
        nombre: "Parihuela de Cabrilla",
        precio: "18.00",
        src: "platos/PARIHUELAS/PARIHUELA-DE-CABRILLA.png"
      },
      {
        nombre: "Parihuela de Filete",
        precio: "15.00",
        src: "platos/PARIHUELAS/PARIHUELA-DE-FILETE.png"
      },
      {
        nombre: "Parihuela de Mariscos",
        precio: "18.00",
        src: "platos/PARIHUELAS/PARIHUELA-DE-MARISCOS.png"
      },
      {
        nombre: "Parihuela de Tramboyo",
        precio: "18.00",
        src: "platos/PARIHUELAS/PARIHUELA-DE-TRAMBOYO.png"
      },
      {
        nombre: "Parihuela de Trucha",
        precio: "18.00",
        src: "platos/PARIHUELAS/PARIHUELA-DE-TRUCHA.png"
      }
    ],
    [
      {
        nombre: "Ceviche + Arroz con Mariscos",
        precio: "20.00",
        src: "platos/PLATOS_DOBLES/CEVICHE-CON-ARROZ-CON-MARISCO.png"
      },
      {
        nombre: "Ceviche + Chaufa de Mariscos",
        precio: "20.00",
        src: "platos/PLATOS_DOBLES/CEVICHE-CON-CHAUFA-DE-MARISCOS.png"
      },
      {
        nombre: "Ceviche + Chicharrón de Pescado",
        precio: "20.00",
        src: "platos/PLATOS_DOBLES/CEVICHE-CON-CHICHARRON-DE-PESCADO.png"
      },
      {
        nombre: "Chicharrón de Pescado + Arroz con Mariscos",
        precio: "20.00",
        src:
          "platos/PLATOS_DOBLES/CHICHARRON-DE-PESCADO-CON-ARROZ-CON-MARISCO.png"
      },
      {
        nombre: "Chicharrón de Pescado + Chaufa de Mariscos",
        precio: "20.00",
        src:
          "platos/PLATOS_DOBLES/CHICHARRON-DE-PESCADO-CON-CHAUFA-DE-MARISCOS.png"
      }
    ],
    [
      {
        nombre: "Porción de Arroz",
        precio: "2.00",
        src: "platos/PORCIONES/PORCION-DE-ARROZ.png"
      },
      {
        nombre: "Porción de Camote",
        precio: "2.00",
        src: "platos/PORCIONES/PORCION-DE-CAMOTE.png"
      },
      {
        nombre: "Porción de Canchita",
        precio: "2.00",
        src: "platos/PORCIONES/PORCION-DE-CANCHITA.png"
      },
      {
        nombre: "Porción de Chifles",
        precio: "2.00",
        src: "platos/PORCIONES/PORCION-DE-CHIFLE.png"
      },
      {
        nombre: "Porción de Ensalada",
        precio: "2.00",
        src: "platos/PORCIONES/PORCION-DE-ENSALADA.png"
      }
    ]
  ],
  listaPedidos: [],
  resumenDialog: false,
  dialogView: "DialogResumen"
});

export const mutations = {
  updateTipoPlatoSeleccionado(state, value) {
    state.tipoPlatoSeleccionado = value;
  },

  addPedido(state, pedido) {
    if (!state.listaPedidos.some(i => i.nombre === pedido.nombre)) {
      state.listaPedidos.push({
        ...pedido,
        cantidad: 1
      });
    }
  },

  showResumenDialog(state) {
    state.resumenDialog = !state.resumenDialog;
  },

  setDialogView(state, value) {
    state.dialogView = value;
  },

  changeCantidadPlato(state, value) {
    state.listaPedidos[value.index]["cantidad"] = value.cantidad;
  },

  removePedido(state, index) {
    state.listaPedidos.splice(index, 1);
  }
};

export const getters = {
  getTipoPlatoSeleccionado: state => {
    return state.tipoPlatoSeleccionado;
  },

  getListaTipoPlatos: state => {
    return state.listaTipoPlatos;
  },

  getListaTipoPlatoSeleccionado: state => {
    return state.listaTipoPlatos[state.tipoPlatoSeleccionado];
  },

  getListaPlatosSeleccionado: state => {
    return state.listaPlatos[state.tipoPlatoSeleccionado];
  },

  getListaPedidos: state => {
    return state.listaPedidos;
  },

  getListaPedidosCounter: state => {
    return state.listaPedidos.length;
  },

  showDialog: state => {
    return state.resumenDialog;
  },

  getDialogView: state => {
    return state.dialogView;
  }
};
